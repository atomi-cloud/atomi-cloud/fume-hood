package main

import (
	"encoding/base64"
	"errors"
	"strings"
)

type Authorization struct {
}

func (a Authorization) Validate(header string) (user string, pass string, err error) {

	// Get the base64
	auth := strings.SplitN(header, " ", 2)

	// Verify that the header was properly formatted
	if !a.CorrectFormat(auth) {
		return "", "", errors.New("incorrect authorization header")
	}

	// decode base64 back to bytes
	bytes, err := base64.StdEncoding.DecodeString(auth[1])
	if err != nil {
		return "", "", err
	}

	// convert bytes back to string
	decoded := string(bytes)

	// Verify is the string is well formted
	user, pass, err = a.ExtractData(decoded)
	if err != nil {
		return "", "", err
	}
	return user, pass, nil
}

func (Authorization) CorrectFormat(s []string) bool {
	return len(s) == 2
}

func (Authorization) ExtractData(decoded string) (user string, pass string, err error) {
	pair := strings.SplitN(decoded, ":", 2)
	if len(pair) != 2 {
		return "", "", errors.New("incorrect encoded data format")
	}
	return pair[0], pair[1], nil
}
