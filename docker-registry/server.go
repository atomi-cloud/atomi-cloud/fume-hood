package main

import (
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
)

func handler(w http.ResponseWriter, r *http.Request) {
	// Logs the format
	req := formatRequest(r) + "\n" + "\n"
	log.Println(req)

	//Set the response header to notify a challenge
	w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)

	// Extract the header
	header := r.Header.Get("Authorization")

	_, namespace, image, err := parseUrl(r.URL)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	// Validate the header
	auth := Authorization{}
	identity := Identity{}
	user, pass, err := auth.Validate(header)

	if err != nil {
		http.Error(w, err.Error(), 401)
		return
	}

	// Verify Username and password
	if !identity.Verify(user, pass, namespace, image) {
		http.Error(w, "Not authorized", 401)
		return
	}

	// Route
	if r.URL.String() == "/v2/" {
		return
	} else {
		registryLink := "http://" + os.Getenv("REGISTRY_PATH") + ":5000"
		reverseProxy(registryLink, w, r)
	}
}

func reverseProxy(target string, res http.ResponseWriter, req *http.Request) {
	link, _ := url.Parse(target)
	proxy := httputil.NewSingleHostReverseProxy(link)

	proxy.ServeHTTP(res, req)
}

func main() {

	http.HandleFunc("/", handler)

	log.Println("Starting...")
	log.Println()

	port := ":" + os.Getenv("AUTH_SERVER_PORT")
	log.Fatal(http.ListenAndServe(port, nil))
}
