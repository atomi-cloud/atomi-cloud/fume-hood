package main

type Identity struct {
}

type User struct {
	username string
	password string
	allowed  string
	valid    []string
}

var users = map[string]User{
	"docker":   {"docker", "docker", "docker", []string{"docker", "badapple"}},
	"kirin":    {"kirin", "kirin", "kirin", []string{"badapple", "ubuntu"}},
	"kirinnee": {"kirinnee", "kirinnee", "kirinnee", []string{"badapple", "alpine", "ubuntu"}},
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
func (Identity) Verify(user string, pass string, namespace string, image string) bool {
	u := users[user]
	if (namespace == "" || image == "") && namespace != image {
		return false
	}
	return u.username == user && u.password == pass && (u.allowed == namespace || namespace == "") && (contains(u.valid, image) || image == "")
}
