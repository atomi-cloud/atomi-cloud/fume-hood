package main

import (
	"errors"
	"net/url"
	"strings"
)

func parseUrl(url *url.URL) (v2 string, namespace string, image string, err error) {

	paths := strings.Split(url.Path, "/")
	paths = where(paths, func(s string) bool { return len(s) != 0 })

	size := len(paths)
	if size == 0 {
		return "", "", "", errors.New("not found")
	} else {
		if paths[0] != "v2" {
			return "", "", "", errors.New("unknown root >>" + paths[0] + "<<")
		}
		if len(paths) == 1 {
			return paths[0], "", "", nil
		} else if len(paths) == 2 {
			return "", "", "", errors.New("denied endpoint")
		} else {
			if paths[3] != "tags" && paths[3] != "manifests" && paths[3] != "blobs" {
				return "", "", "", errors.New("unknown endpoint")
			} else {
				return paths[0], paths[1], paths[2], nil
			}
		}
	}
}
