package main

import (
	"fmt"
	"net/http"
	"strings"
)

func formatRequest(r *http.Request) string {
	// Create return string
	var request []string
	// Add the request string
	link := fmt.Sprint("VERB: ", r.Method, " ENDPOINT: ", r.URL, " PROTOCOL: ", r.Proto)
	request = append(request, link)
	// Add the host
	request = append(request, fmt.Sprint("Host: ", r.Host))
	// Loop through headers
	for name, headers := range r.Header {
		name = strings.ToLower(name)
		for _, h := range headers {
			request = append(request, fmt.Sprint(name, ": ", h))
		}
	}

	// If this is a POST, add post data
	if r.Method == "POST" {
		r.ParseForm()
		request = append(request, "\n")
		request = append(request, r.Form.Encode())
	}
	// Return the request as a string
	return strings.Join(request, "\n")
}
